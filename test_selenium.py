from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
import time

serviceChrome=Service(executable_path="chromedriver.exe")
driver =webdriver.Chrome(service=serviceChrome)
driver.get("https://www.google.com/")
search_box=driver.find_element('name','q')
search_box.send_keys("python selenium")
search_box.submit()
search_results = driver.find_elements(By.CSS_SELECTOR, "div.g")[:11]
time.sleep(2)

for index, res in enumerate(search_results, start=1):
    title = res.find_element(By.CSS_SELECTOR, "h3").text
    url = res.find_element(By.XPATH, ".//a").get_attribute("href")
    print(f"{index}. Title: {title}")
    print(f"   URL: {url}\n")

driver.quit()